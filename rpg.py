import libtcodpy as libtcod
import random

def write(console, x, y, string, colour = libtcod.white):
    libtcod.console_set_default_foreground(0, colour)
    libtcod.console_print(console, x, y, string)
    libtcod.console_set_default_foreground(0, libtcod.white)

class Skill:
    all_skills = []
    def __init__(self, name):
        self.name = name
        self.rank = 1
        self.aptitude = 0
        self.exp = 0
        Skill.all_skills.append(self)

    def to_letter(self):
        if self.aptitude <= -3: return 'F'
        if self.aptitude == -2: return 'E'
        if self.aptitude == -1: return 'D'
        if self.aptitude ==  0: return 'C'
        if self.aptitude ==  1: return 'B'
        if self.aptitude ==  2: return 'A'
        if self.aptitude ==  3: return 'S'
        if self.aptitude >=  4: return 'SS'

    def to_mod(self):
        if self.aptitude <= -3: return 0.2
        if self.aptitude == -2: return 0.5
        if self.aptitude == -1: return 0.8
        if self.aptitude ==  0: return 1
        if self.aptitude ==  1: return 1.2
        if self.aptitude ==  2: return 1.4
        if self.aptitude ==  3: return 1.8
        if self.aptitude >=  4: return 2

    def train(self, amount):
        self.exp += amount
        if self.exp >= self.rank * 50:
            self.rank += 1
        print "{0} E:{1} R{2}".format(self.name, self.exp, self.rank)


skillSword = Skill("Sword")
skillMagic = Skill("Magic")

class Shard:
    core_shards = []
    racial_shards = []
    spec_shards = []
    def __init__(self, name, pos_aptitude, neg_aptitude, prerequisite, racial = False):
        self.name = name
        self.pos_aptitude = pos_aptitude
        self.neg_aptitude = neg_aptitude
        self.prerequisite = prerequisite
        self.racial = racial
        self.level = 1
        self.exp = 0

    def train(self, amount):
        self.exp += amount
        if self.exp >= self.level * 50:
            self.level += 1
        print "{0} E:{1} L{2}".format(self.name, self.exp, self.level)

    @staticmethod
    def total():
        return len(Shard.core_shards + Shard.racial_shards + Shard.spec_shards)

class CoreShard(Shard):
    def __init__(self, name, pos_aptitude, neg_aptitude):
        Shard.__init__(self, name, pos_aptitude, neg_aptitude, None)
        Shard.core_shards.append(self)

class SpecialistShard(Shard):
    def __init__(self, name, pos_aptitude, neg_aptitude, prerequisite):
        Shard.__init__(self, name, pos_aptitude, neg_aptitude, prerequisite)
        Shard.spec_shards.append(self)

class RacialShard(Shard):
    def __init__(self, name):
        Shard.__init__(self, name, [], [], None, True)
        Shard.racial_shards.append(self)

shardFighter = CoreShard("Fighter", [skillSword], [skillMagic])
shardSorcerer = CoreShard("Sorcerer", [skillMagic], [skillSword])
shardKnight = SpecialistShard("Knight", [skillSword, skillSword], [skillMagic], [shardFighter])
shardWarMage = SpecialistShard("WarMage", [skillSword, skillMagic], [], [shardFighter, shardSorcerer])
shardTroll = RacialShard("Troll")
shardSlugman = RacialShard("Slugman")

key_to_shard = {}

class Combatant:
    def __init__(self):
        self.hp = self.max_hp = 50

    def take_damage(self, amount):
        self.hp -= amount
        if self.hp > self.max_hp:
            self.hp = self.max_hp

class Enemy(Combatant):
    def __init__(self):
        Combatant.__init__(self)
        self.reset()

    def reset(self):
        self.hp = self.max_hp = 20

enemy = Enemy()

class Ability:
    def __init__(self, name, skill):
        self.name = name
        self.skill = skill

abilitySlash = Ability("Slash", skillSword)
abilityFireBall = Ability("Fire ball", skillMagic)

class Player(Combatant):
    def __init__(self):
        Combatant.__init__(self)
        self.shards = []
        self.gold = 0
        self.skills = Skill.all_skills
        self.abilities = {'1' : abilitySlash,
                          '2' : abilityFireBall}

    def change_shard(self, key):
        shard = key_to_shard[key]
        try:
            self.remove_shard(shard)
            for other_shard in self.shards:
                if other_shard.prerequisite != None:
                    if shard in other_shard.prerequisite:
                        self.remove_shard(other_shard)
        except ValueError:
            if len(self.shards) == 3: return
            if shard.racial == True:
                for other_shard in self.shards:
                    if other_shard.racial == True:
                        return
            if shard.prerequisite != None:
                for required_shard in shard.prerequisite:
                    if not required_shard in self.shards:
                        return
            self.add_shard(shard)

    def add_shard(self, shard):
        self.shards.append(shard)
        for skill in shard.pos_aptitude:
            skill.aptitude += 1
        for skill in shard.neg_aptitude:
            skill.aptitude -= 1

    def remove_shard(self, shard):
        self.shards.remove(shard)
        for skill in shard.pos_aptitude:
            skill.aptitude -= 1
        for skill in shard.neg_aptitude:
            skill.aptitude += 1

    def has_shard(self, shard):
        return shard in self.shards

    def print_shards(self):
        shards_to_print = []
        for shard in self.shards:
            if shard.prerequisite != None:
                for prerequisite in shard.prerequisite:
                    shards_to_print.remove(prerequisite)
            shards_to_print.append(shard)

        racial = "Human"
        special = ""
        core = ""
        for shard in shards_to_print:
            if shard.racial == True:
                racial = shard.name
            elif shard.prerequisite != None:
                special = " " + shard.name
            else:
                core += shard.name + " "
        return "{0}{1} {2}".format(racial, special, core)

    def gain_gold(self, amount):
        self.gold += amount

    def use_ability(self, ability):
        damage = 5
        aptitude_mod = ability.skill.to_mod()
        damage *= aptitude_mod
        ability.skill.train(5 * aptitude_mod)
        for shard in self.shards:
            if ability.skill in shard.pos_aptitude:
                shard.train(10)
            if shard.racial == True:
                shard.train(5)
        return damage

player = Player()

state = []

class StateBattleWin():
    def render(self):
        write(0, 0, 1, "You win!")

    def input(self, key):
        state.pop()

class StateBattle:
    def __init__(self):
        self.player_action = ""
        self.enemy_action = ""

    def render(self):
        write(0, 0, 1 ,"HP: {0}/{1}".format(player.hp, player.max_hp))
        write(0, 0, 2, "Enemy HP: {0}/{1}".format(enemy.hp, enemy.max_hp))

        write(0, 0, 4, self.player_action)
        write(0, 0, 5, self.enemy_action)

        i = 0
        for key, ability in player.abilities.items():
            write(0, 0, 7+i, "[{0}] {1}".format(key, ability.name))
            i+=1

    def input(self, key):
        ability = player.abilities.get(chr(key.c))
        if ability != None:
            damage = int(player.use_ability(ability))
            enemy.take_damage(damage)
            self.player_action = "You attack with {0} for {1} damage".\
                                  format(ability.name, damage)
        else:
            return

        if enemy.hp <= 0:
            state.pop()
            state.append(StateBattleWin())
        else:
            player.take_damage(5)
            self.enemy_action = "The enemy attacks for 5 damage"

class StateTreasureChest:
    def render(self):
        write(0, 0, 1, "You find a treasure chest with 20 gold in it!")

    def input(self, key):
        player.gain_gold(20)
        state.pop()

class StateStats:
    def render(self):
        write(0, 0, 1, "You are a {0}".format(player.print_shards()))
        write(0, 0, 2, "HP: {0}/{1}".format(player.hp, player.max_hp))
        for i, shard in enumerate(player.shards):
            write(0, 0, 3+i, "{0} - {1}".format(shard.level, shard.name))

        write(0, 0, 7, "Skills")
        for i, skill in enumerate(player.skills):
            x = i % 5 * 16
            y = ((i+1) / 5) + 8
            write(0, x, y, "{0}({1}) {2}".format(skill.to_letter(), skill.rank, skill.name))

    def input(self, key):
        state.pop()

class StateDungeon:
    def render(self):
        write(0, 0, 1, "HP: {0}/{1} Gold:{2}".
            format(player.hp, player.max_hp, player.gold))
        write(0, 0, 3, "[w] Wander")
        write(0, 0, 4, "[r] Rest")
        write(0, 0, 5, "[s] Stats")

    def input(self, key):
        if key.c == ord('w'):
            rand = random.randint(1, 10)
            if rand <= 2:
                pass
            elif rand <= 5:
                pass
            elif rand <= 9:
                enemy.reset()
                state.append(StateBattle())
            else:
                state.append(StateTreasureChest())
        if key.c == ord('r'):
            rand = random.randint(1, 10)
            if rand <= 9:
                player.take_damage(-player.max_hp)
            else:
                enemy.reset()
                state.append(StateBattle())
        if key.c == ord('s'):
            state.append(StateStats())

class StateCharacterCreate:
    def render(self):
        type_to_shards = [["Core", Shard.core_shards],
                          ["Race", Shard.racial_shards],
                          ["Specialisation", Shard.spec_shards]]

        letter = ord('a')
        for index, [category, shards] in enumerate(type_to_shards):
            write(0, index * 16, 1, category)
            for (y, shard) in enumerate(shards):
                libtcod.console_print(0, index * 16, 2+y, chr(letter))
                colour = libtcod.white
                if player.has_shard(shard):
                    colour = libtcod.red
                if shard.prerequisite != None:
                    for prerequisite in shard.prerequisite:
                        if not player.has_shard(prerequisite):
                            colour = libtcod.grey
                            break
                write(0, index * 16+2, 2+y, shard.name, colour)
                key_to_shard[letter] = shards[y]
                letter += 1

        write(0, 0, 15, "You are a " + player.print_shards())
        if len(player.shards) == 3:
            write(0, 0, 17, "Press enter to continue")

        write(0, 0, 19, "Skill aptitudes")
        for i, skill in enumerate(player.skills):
            x = i % 5 * 16
            y = ((i+1) / 5) + 20
            write(0, x, y, "{0} {1}".format(skill.to_letter(), skill.name))

    def input(self, key):
        if ord('a') <= key.c < ord('a') + Shard.total():
            player.change_shard(key.c)
        if key.vk == libtcod.KEY_ENTER and len(player.shards) == 3:
            state.pop()
            state.append(StateDungeon())

SCREEN_WIDTH = 80
SCREEN_HEIGHT = 50

libtcod.console_set_custom_font('terminal.png', libtcod.FONT_TYPE_GRAYSCALE | libtcod.FONT_LAYOUT_TCOD)
libtcod.console_init_root(SCREEN_WIDTH, SCREEN_HEIGHT, 'rpg', False)
libtcod.console_set_default_foreground(0, libtcod.white)
state = [StateCharacterCreate()]

while not libtcod.console_is_window_closed():
    libtcod.console_clear(0)

    state[-1].render()
    libtcod.console_flush()
    key = libtcod.console_wait_for_keypress(True)
    if key.pressed == False: continue
    if key.c == ord('q'):
        break
    state[-1].input(key)
